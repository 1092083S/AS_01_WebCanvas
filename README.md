# Software Studio 2021 Spring
## Assignment 01 Web Canvas


### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | Y         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Y         |
| Un/Re-do button                                  | 10%       | Y         |
| Image tool                                       | 5%        | Y         |
| Download                                         | 5%        | Y         |

| **Other useful widgets**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| rainbow pen                                      | 1~5%      | Y         |
| dot pen                                          | 1~5%      | Y         |
| rectangle pen                                    | 1~5%      | Y         |


---

### How to use 

#### 小畫家畫面
* 左邊是工具列
* 右邊全部空間都是畫布

![](https://i.imgur.com/y2C9Bdu.png)

----

#### 工具列區塊(1)
* 在這裡可以選擇筆刷大小
* 小、中、大，可直接點擊選擇

![](https://i.imgur.com/zKxFcm7.png)

----

#### 工具列區塊(2)
* 在這裡可以選擇筆刷顏色
* 第一種方式：可以直接點擊，使用預設顏色，共六款
* 第二種方式：使用自選色，可以隨意挑選

![](https://i.imgur.com/5L5YKwa.png)

----

#### 工具列區塊(3-1)
* 在這裡可以選擇想要的功能
* 可以同時選擇想要的顏色
    1. 一般畫筆 (可選筆刷大小、顏色)
    2. 橡皮擦 (可選筆刷大小、顏色)
    3. 畫圓形+橢圓 (可選顏色)
    4. 畫矩形 (可選顏色)
    5. 畫三角形 (可選顏色)
    6. 多做的功能，放在後面說明
    7. 多做的功能，放在後面說明
    8. 多做的功能，放在後面說明
    9. 文字方塊 (可選顏色)

![](https://i.imgur.com/Ts2oQMa.png)

----

#### 工具列區塊(4)
* 在這裡可以選擇文字框的大小、字型
* 文字方塊使用方法：
點擊後選畫布上一個地方會出現文字框，在裡面打字後，選擇顏色、大小、字型
最後按下Enter就完成了

![](https://i.imgur.com/DUog1zq.png)

----

#### 工具列區塊(5)
* 在這裡可以選擇動作
    1. 清空整個畫布
    2. 回到上一步
    3. 重作一步
    4. 上傳圖片至整個畫布
    5. 下載整個畫布

![](https://i.imgur.com/FTCshTm.png)

----
### Function description

#### 工具列區塊(3-2)
* 在這裡補充上述678點
* 可以選擇想要的功能
* 可以同時選擇想要的顏色
    6. 彩虹筆 (可選筆刷大小)
    7. 點點筆 (可選顏色)
    8. 方塊筆 (可選顏色)

      
![](https://i.imgur.com/WZbcwO3.png)

----

### Gitlab page link

    your web page URL, which should be "https://1092083S.gitlab.io/AS_01_WebCanvas"

### Others (Optional)

    Anythinh you want to say to TAs.

    Thank you!

<style>
table th{
    width: 100%;
}
</style>