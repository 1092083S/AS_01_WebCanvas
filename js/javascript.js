// 變數設定
var myCanvas = document.getElementById("myCanvas");
var context = myCanvas.getContext("2d");
var colorSelector = document.getElementById("colorSelector");
var undoBtn = document.getElementById("undoBtn");
var redoBtn = document.getElementById("redoBtn");
var downloadBtn = document.getElementById('downloadBtn');
var uploadBtn = document.getElementById('uploadBtn');
var textModeBtn = document.getElementById('textModeBtn');
var inputfontSize = document.getElementById('fontSize');
var selecfontFamily = document.getElementById('fontFamily');


// 創建textInput
var textInput = document.createElement('input');
var body = document.getElementsByTagName('body')[0];
body.appendChild(textInput);
textInput.id = 'textInput';
textInput.style.display = 'none';
textInput.style.background = '#ffffff5c';

var x = 0;
var y = 0;
var x1 = 0;
var y1 = 0;
var x2 = 0;
var y2 = 0;
var clientX = 0;
var clientY = 0;
var drawing = false;
var canvasArray = [];
var currentPictureIndex = 0;

// 設定畫布大小
function resizeCanvas() {
  myCanvas.width = document.body.clientWidth - 200;
  myCanvas.height = document.body.clientHeight;
  takePicture();
} 
resizeCanvas();


// 監聽區
myCanvas.addEventListener('mousedown', startDrawLine);
myCanvas.addEventListener('mousemove', isDrawLine);
myCanvas.addEventListener('mouseup', endDrawLine);
myCanvas.addEventListener('mouseleave', endDrawLine);

colorSelector.addEventListener('change', updateBrushColorBySelector);
uploadBtn.addEventListener('change', toUpload);


// 筆刷設定
var brushSize = 3;
var brushColor = '#000000';
var brushFunction = 'pen';
var fontSize = 16;
var fontFamily = '新細明體';

function updateBrushSize(size) {
  brushSize = size;
}
function updateBrushColor(color) {
  // 如果有指定顏色的話
  brushColor = color;
  colorSelector.value = brushColor;
}
function updateBrushColorBySelector() {
  // 如果是colorSelector.value在動的話
  brushColor = colorSelector.value;
}
function updateBrushFunction(brush) {
  if (brush === 'pen') {
    brushFunction = 'pen';
    myCanvas.style.cursor = "url(./assets/cursor-pen.png) 0 15, auto";
  }
  if (brush === 'eraser') {
    brushFunction = 'eraser';
    myCanvas.style.cursor = "url(./assets/cursor-eraser.png) 0 15, auto";
  }
  if (brush === 'dotPen') {
    brushFunction = 'dotPen';
    myCanvas.style.cursor = "url(./assets/cursor-circle.png), auto";
  }
  if (brush === 'rectanglePen') {
    brushFunction = 'rectanglePen';
    myCanvas.style.cursor = "url(./assets/cursor-rectangle.png), auto";
  }
  if (brush === 'rectangle') {
    brushFunction = 'rectangle';
    myCanvas.style.cursor = "url(./assets/cursor-rectangle.png), auto";
  }
  if (brush === 'circle') {
    brushFunction = 'circle';
    myCanvas.style.cursor = "url(./assets/cursor-circle.png), auto";
  }
  if (brush === 'triangle') {
    brushFunction = 'triangle';
    myCanvas.style.cursor = "url(./assets/cursor-triangle.png), auto";
  }
  if (brush === 'textMode') {
    brushFunction = 'textMode';
    myCanvas.style.cursor = 'text';
  }
  if (brush === 'rainbowPen') {
    brushFunction = 'rainbowPen';
    myCanvas.style.cursor = "url(./assets/cursor-pen.png), auto";
  }
}
function updateFontSize(size) {
  fontSize = size;
  textInput.style.fontSize = size + 'px';
}
function updateFontFamily(font) {
  fontFamily = font;
  textInput.style.fontFamily = font;
}


// 畫畫動作
function startDrawLine(event) {
  x1 = event.offsetX;
  y1 = event.offsetY;
  x = event.offsetX;
  y = event.offsetY;
  clientX = event.clientX;
  clientY = event.clientY;

  drawing = true;
  if (brushFunction === 'textMode') {
    setTimeout(function(){
      writeText(clientX, clientY, x1, y1);
    }, 10);
  }
}
function isDrawLine(event) {
  if (drawing === true) {
    x2 = event.offsetX;
    y2 = event.offsetY;
    if (brushFunction === 'pen') {
      drawLine(x1, y1, x2, y2);
    }
    if (brushFunction === 'rainbowPen') {
      rainbowLine(x1, y1, x2, y2);
    }
    if (brushFunction === 'eraser') {
      eraseLine(x1, y1, x2, y2);
    }
    if (brushFunction === 'dotPen') {
      drawDotLine(x1, y1, x2, y2);
    }
    if (brushFunction === 'rectanglePen') {
      drawRectangleLine(x1, y1, x2, y2);
    }
    if (brushFunction === 'rectangle') {
      makeRectangle(x, y, x2, y2);
    }
    if (brushFunction === 'circle') {
      makeCircle(x, y, x2, y2);
    }
    if (brushFunction === 'triangle') {
      makeTriangle(x, y, x2, y2);
    }
    x1 = event.offsetX;
    y1 = event.offsetY;
  }
}
function endDrawLine(event) {
  if (drawing === true) {
    x2 = event.offsetX;
    y2 = event.offsetY;
    if (brushFunction === 'pen') {
      drawLine(x1, y1, x2, y2);
    }
    if (brushFunction === 'rainbowPen') {
      rainbowLine(x1, y1, x2, y2);
    }
    if (brushFunction === 'eraser') {
      eraseLine(x1, y1, x2, y2);
    }
    if (brushFunction === 'dotPen') {
      drawDotLine(x1, y1, x2, y2)
    }
    if (brushFunction === 'rectanglePen') {
      drawRectangleLine(x1, y1, x2, y2);
    }
    x1 = 0;
    y1 = 0;
    x = 0;
    y = 0;
    takePicture()
    context.globalCompositeOperation = 'source-over';
    drawing = false;
  }
}


// 畫畫功能
function drawLine(x1,y1,x2,y2) {
  context.beginPath();
  context.lineCap = 'round';
  context.lineJoin = 'round';
  context.strokeStyle = brushColor;
  context.lineWidth = brushSize;
  context.moveTo(x1,y1);
  context.lineTo(x2,y2);
  context.stroke();
  context.closePath();
}
function rainbowLine(x1,y1,x2,y2) {
  context.beginPath();
  context.lineCap = 'round';
  context.lineJoin = 'round';
  var gradient = context.createLinearGradient(0, 0, myCanvas.width, myCanvas.height);
  gradient.addColorStop(0.1, "blue");
  gradient.addColorStop(0.3, "green");
  gradient.addColorStop(0.6, "yellow");
  gradient.addColorStop(1.0, "red");
  context.strokeStyle = gradient;
  context.lineWidth = brushSize;
  context.moveTo(x1,y1);
  context.lineTo(x2,y2);
  context.stroke();
  context.closePath();
}
function eraseLine(x1,y1,x2,y2) {
  context.globalCompositeOperation = 'destination-out';
  drawLine(x1,y1,x2,y2);
}
function drawDotLine(x1, y1, x2, y2) {
  var r = Math.sqrt(Math.abs(x1-x2)+Math.abs(y1-y2));
  context.beginPath();
  context.fillStyle = brushColor;
  context.strokeStyle = brushColor;
  context.arc(x1, y1, r, 0, 2*Math.PI);
  context.fill();
}
function drawRectangleLine(x1,y1, x2, y2) {
  context.beginPath();
  context.fillStyle = brushColor;
  context.strokeStyle = brushColor;
  context.rect(x1,y1,Math.abs(x2-x1),Math.abs(y2-y1));
  context.fill();
}
function makeRectangle(x, y, x2, y2) {
  var img = new Image();
  img.onload = function() {
    context.clearRect(0, 0, myCanvas.width, myCanvas.height);
    context.drawImage(img, 0, 0, myCanvas.width, myCanvas.height);
    context.beginPath();
    context.fillStyle = brushColor;
    context.fillRect(x, y, x2-x, y2-y);
  }
  img.src = canvasArray[currentPictureIndex];
}
function makeCircle(x, y, x2, y2) {
  var img = new Image();
  img.onload = function() {
    context.clearRect(0, 0, myCanvas.width, myCanvas.height);
    context.drawImage(img, 0, 0, myCanvas.width, myCanvas.height);
    context.beginPath();
    var radiusX = Math.abs((x2-x))/2;
    var radiusY = Math.abs((y2-y))/2;
    context.fillStyle = brushColor;
    context.ellipse((x+x2)/2, (y+y2)/2, radiusX, radiusY, 0, 0, 2 * Math.PI);
    context.fill();
  }
  img.src = canvasArray[currentPictureIndex];
}
function makeTriangle(x, y, x2, y2) {
  var img = new Image();
  img.onload = function() {
    context.clearRect(0, 0, myCanvas.width, myCanvas.height);
    context.drawImage(img, 0, 0, myCanvas.width, myCanvas.height);
    
    context.lineCap = 'round';
    context.lineJoin = 'round';
    context.fillStyle = brushColor; // 實心的
    // context.strokeStyle = brushColor; // 空心的
    context.lineWidth = brushSize;

    context.beginPath();

    context.moveTo(x, y);
    context.lineTo(x2, y2);
    context.lineTo(x2+ (x-x2)*2, y2);
    context.lineTo(x, y);

    context.fill(); // 實心的
    // context.stroke(); // 空心的

    context.closePath();

  }
  img.src = canvasArray[currentPictureIndex];
}



// 打字文字框
function writeText(clientX, clientY, x1, y1) {
  textInput.style.display = 'block';
  textInput.value = '';
  textInput.focus();
  body.style.position = 'relative';
  textInput.style.position = 'absolute';
  textInput.style.top = clientY+'px';
  textInput.style.left = clientX+'px';
  textInput.onkeydown = (e)=>{
    if (e.key === 'Enter') {
      context.fillStyle = brushColor;
      context.font = `${fontSize}`+'px '+`${fontFamily}`;
      context.fillText(textInput.value, x1, y1+Number(fontSize));
      textInput.style.display = 'none';
      takePicture()
    }
  }
}
textInput.addEventListener('blur', removeTextInput);
function removeTextInput(e) {
  if (brushFunction === 'textMode') {
    if (e.relatedTarget === inputfontSize) {
      console.log('成功');
    }
    else if (e.relatedTarget === selecfontFamily) {
      console.log('成功');
    }
    else if (e.relatedTarget === brushColorBtn[0]) {
      console.log('成功');
    }
    else if (e.relatedTarget === brushColorBtn[1]) {
      console.log('成功');
    }
    else if (e.relatedTarget === brushColorBtn[2]) {
      console.log('成功');
    }
    else if (e.relatedTarget === brushColorBtn[3]) {
      console.log('成功');
    }
    else if (e.relatedTarget === brushColorBtn[4]) {
      console.log('成功');
    }
    else if (e.relatedTarget === brushColorBtn[5]) {
      console.log('成功');
    }
    else if (e.relatedTarget === colorSelector) {
      console.log('成功');
    }
    else {
      textInput.style.display = 'none';
    }
  } else {
    textInput.style.display = 'none';
  }
}



// 清除功能
function clearCanvas() {
  context.clearRect(0, 0, myCanvas.width, myCanvas.height);
  takePicture()
}

// 上一步及重做
function undo() {
  if (currentPictureIndex > 0) {
    currentPictureIndex = currentPictureIndex - 1;
    var img = new Image();
    img.onload = function() {
      context.clearRect(0, 0, myCanvas.width, myCanvas.height);
      context.drawImage(img, 0, 0, myCanvas.width, myCanvas.height);
    }
    img.src = canvasArray[currentPictureIndex];
  }
  if (currentPictureIndex === 0) {
    undoBtn.disabled = true;
  }
  if (currentPictureIndex !== canvasArray.length - 1) {
    redoBtn.disabled = false; 
  }
}
function redo() {
  if (currentPictureIndex < canvasArray.length - 1) {
    currentPictureIndex = currentPictureIndex + 1;
    var img = new Image();
    img.onload = function() {
      context.clearRect(0, 0, myCanvas.width, myCanvas.height);
      context.drawImage(img, 0, 0, myCanvas.width, myCanvas.height);
    }
    img.src = canvasArray[currentPictureIndex];
  }
  if (currentPictureIndex === canvasArray.length - 1) {
    redoBtn.disabled = true;
  }
  if (canvasArray.length > 1) {
    undoBtn.disabled = false;
  }
}

// 截圖
function takePicture() {
  redoBtn.disabled = true; 
  canvasArray = canvasArray.slice(0 , currentPictureIndex+1);
  var canvasPicture = myCanvas.toDataURL();
  if (canvasPicture !== canvasArray[canvasArray.length - 1]) {
    canvasArray.push(canvasPicture);
    currentPictureIndex = canvasArray.length - 1;
    if (canvasArray.length > 1) {
      undoBtn.disabled = false;
    }
  }
}



// 下載及上傳
function toUpload() {
  var img = new Image();
  img.onload = function() {
    context.drawImage(img, 0, 0, myCanvas.width, myCanvas.height);
  }
  var file = uploadBtn.files[0];
  img.src = URL.createObjectURL(file);

  
}
function toDownload() {
  var canvasPicture = myCanvas.toDataURL();
  downloadBtn.href = canvasPicture;
}


// Hightlight Btn
var brushBtn = document.getElementsByClassName('brushBtn');
for (let index = 0; index < brushBtn.length; index++) {
  const element = brushBtn[index];
  element.addEventListener('click', checkBrush);
}
function checkBrush(event) {
  for (let index = 0; index < brushBtn.length; index++) {
    const item = brushBtn[index];
    item.style.border = 'dotted 2px #ccc';
    item.style.background = 'transparent';

  }
  event.currentTarget.style.border = 'solid 2px #555';
  event.currentTarget.style.background = '#ecebbd50';
}
var brushColorBtn = document.getElementsByClassName('brushColorBtn');
for (let index = 0; index < brushColorBtn.length; index++) {
  const element = brushColorBtn[index];
  element.addEventListener('click', checkBrushColorBtn);
}
function checkBrushColorBtn(event) {
  for (let index = 0; index < brushColorBtn.length; index++) {
    const item = brushColorBtn[index];
    item.style.border = 'dotted 2px #ccc';
    item.style.background = 'transparent';

  }
  event.currentTarget.style.border = 'solid 2px #555';
  event.currentTarget.style.background = '#ecebbd50';
}
var brushFunctionBtn = document.getElementsByClassName('brushFunctionBtn');
for (let index = 0; index < brushFunctionBtn.length; index++) {
  const element = brushFunctionBtn[index];
  element.addEventListener('click', checkBrushFunctionBtn);
}
function checkBrushFunctionBtn(event) {
  for (let index = 0; index < brushFunctionBtn.length; index++) {
    const item = brushFunctionBtn[index];
    item.style.border = 'dotted 2px #ccc';
    item.style.background = 'transparent';

  }
  event.currentTarget.style.border = 'solid 2px #555';
  event.currentTarget.style.background = '#ecebbd50';
}
